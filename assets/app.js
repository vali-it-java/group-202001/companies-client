let currentlyActiveCompanyId = 0;

// Initialization after the HTML document has been loaded...
window.addEventListener('DOMContentLoaded', () => {

    // Function calls for initial page loading activities...
    doLoadCompanies();
});

/* 
    --------------------------------------------
    ACTION FUNCTIONS
    --------------------------------------------
*/

async function doLoadCompanies() {
    console.log('Loading companies...');
    let companies = await fetchCompanies();
    displayCompanies(companies);
}

async function doDeleteCompany(id) {
    if (confirm('Soovid sa tõesti seda ettevõtet kustutada?')) {
        await deleteCompany(id);
        doLoadCompanies();
    }
}

async function doSaveCompany() {
    console.log('Saving company...');
    let errors = validateCompanyEditForm();
    if (errors.length == 0) {
        let myCompany = {
            id: currentlyActiveCompanyId,
            name: document.querySelector('#companyNameInput').value,
            logo: document.querySelector('#companyLogoInput').value,
            employees: document.querySelector('#companyEmployeesInput').value
        };
        await saveCompany(myCompany);
        await doLoadCompanies();
        closePopup();
    } else {
        // kuva veateated
        displayErrors(errors);
    }
}

/* 
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

function displayCompanies(companies) {
    let companiesHtml = ``;
    for (let i = 0; i < companies.length; i++) {
        companiesHtml = companiesHtml + /*html*/`
            <tr>
                <td>${companies[i].name}</td>
                <td>
                    <img src="${companies[i].logo}" height="20">
                </td>
                <td>${companies[i].employees}</td>
                <td>
                    <button class="button-red" onclick="doDeleteCompany(${companies[i].id})">Kustuta</button>
                    <button onclick="displayCompanyEditPopup(${companies[i].id})">Muuda</button>
                </td>
            </tr>
        `;
    }
    document.querySelector('#companiesListRows').innerHTML = companiesHtml;
}

async function displayCompanyEditPopup(id) {
    await openPopup(POPUP_CONF_COMPANY_EDIT, 'companyEditFormTemplate');

    if (id > 0) {
        // Muutmine
        let company = await fetchCompany(id);
        document.querySelector('#companyNameInput').value = company.name;
        document.querySelector('#companyLogoInput').value = company.logo;
        document.querySelector('#companyEmployeesInput').value = company.employees;
        currentlyActiveCompanyId = company.id;
    } else {
        // Lisamine
        currentlyActiveCompanyId = 0;
    }
}

// Validation functions

function validateCompanyEditForm() {
    let errors = [];

    if (document.querySelector('#companyNameInput').value == null ||
        document.querySelector('#companyNameInput').value.length < 2 ||
        document.querySelector('#companyNameInput').value.length > 100) {
        errors.push('Ettevõtte nimi puudu või vale pikkusega!');
    }

    if (document.querySelector('#companyLogoInput').value == null ||
        document.querySelector('#companyLogoInput').value.length < 1 ||
        document.querySelector('#companyLogoInput').value.length > 255 ||
        !document.querySelector('#companyLogoInput').value.startsWith("http")) {
        errors.push('Ettevõtte logo puudu või ebakorrektne URL!');
    }

    if (document.querySelector('#companyEmployeesInput').value == null ||
        document.querySelector('#companyEmployeesInput').value < 1) {
        errors.push('Ettevõtte töötajaid peab olema vähemalt üks!');
    }

    return errors;
}

function displayErrors(errors) {
    if (errors.length > 0) {
        let errorsHtml = '';
        for (let i = 0; i < errors.length; i++) {
            errorsHtml = errorsHtml + `
            <div>${errors[i]}</div>
        `;
        }
        document.querySelector('#errorBox').innerHTML = errorsHtml;
        document.querySelector('#errorBox').style.display = 'block';
    } else {
        document.querySelector('#errorBox').style.display = 'none';
    }
}

function liveDisplayErrors() {
    let errors = validateCompanyEditForm();
    displayErrors(errors);
}